﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biometria__Zadania
{
    /// <summary>
    /// Logika interakcji dla klasy RGBSelector.xaml
    /// </summary>
    public partial class RGBSelector : Window
    {
        public System.Drawing.Color myColor { get; set; }
        public RGBSelector(System.Drawing.Color _myColor)
        {
            InitializeComponent();
            InitializeComponent();
            myColor = _myColor;

            textBlock_B.Text = (myColor.B).ToString();
            textBlock_G.Text = (myColor.G).ToString();
            textBlock_R.Text = (myColor.R).ToString();

        }

        private void confirm_btn_Click(object sender, RoutedEventArgs e)
        {
            int r, g, b;
            try
            {
                r = Int32.Parse(textBlock_R.Text);
                g = Int32.Parse(textBlock_G.Text);
                b = Int32.Parse(textBlock_B.Text);
                myColor = System.Drawing.Color.FromArgb(myColor.A, r, g, b);
                this.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
        }


        private void textBlock_TextChanged(object sender, TextChangedEventArgs e)
        {
            int r, g, b;
            try
            {
                r = Int32.Parse(textBlock_R.Text);
                g = Int32.Parse(textBlock_G.Text);
                b = Int32.Parse(textBlock_B.Text);
                myColor = System.Drawing.Color.FromArgb(myColor.A, r, g, b);
                RGB_color.Fill = new SolidColorBrush(Color.FromRgb(myColor.R, myColor.G, myColor.B));
            }
            catch (Exception ex)
            {
                Console.WriteLine("IOException source: {0}", ex.Source);
            }
        }

        private void white_Click(object sender, RoutedEventArgs e)
        {
            textBlock_B.Text = "255";
            textBlock_G.Text = "255";
            textBlock_R.Text = "255";
        }

        private void black_Click(object sender, RoutedEventArgs e)
        {
            textBlock_B.Text = "0";
            textBlock_G.Text = "0";
            textBlock_R.Text = "0";
        }
    }
}
