﻿using System;
using System.Windows;

namespace Biometria__Zadania
{
    /// <summary>
    /// Logika interakcji dla klasy MaskWindow.xaml
    /// </summary>
    public partial class MaskWindow : Window
    {
        private readonly MaskLogic mask= new MaskLogic();
        private int[,] Mask3x3 = new int[3, 3];
        public MaskWindow()
        {
          //  mask = new MaskLogic();
            InitializeComponent();
        }

        private void Confirm_Mask(object sender, RoutedEventArgs e)
        {
            Mask3x3[0, 0]= Convert.ToInt32(Table_1_1.Text);
            Mask3x3[0, 1] = Convert.ToInt32(Table_1_2.Text);
            Mask3x3[0, 2] = Convert.ToInt32(Table_1_3.Text);

            Mask3x3[1, 0] = Convert.ToInt32(Table_2_1.Text);
            Mask3x3[1, 1] = Convert.ToInt32(Table_2_2.Text);
            Mask3x3[1, 2] = Convert.ToInt32(Table_2_3.Text);

            Mask3x3[2, 0] = Convert.ToInt32(Table_3_1.Text);
            Mask3x3[2, 1] = Convert.ToInt32(Table_3_2.Text);
            Mask3x3[2, 2] = Convert.ToInt32(Table_3_3.Text);

            ((MainWindow)System.Windows.Application.Current.MainWindow).EditImage.Source = Filtry.ManualMask(Mask3x3,3);
            Close();
        }

    }
}
