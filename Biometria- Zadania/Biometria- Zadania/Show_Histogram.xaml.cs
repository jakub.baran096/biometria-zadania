﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biometria__Zadania
{
    /// <summary>
    /// Logika interakcji dla klasy Show_Histogram.xaml
    /// </summary>
    public partial class Show_Histogram : Window
    {
        private BitmapSource image_his { get; set; }
        private BitmapSource image_his_ext { get; set; }

        public int[] HistogramRed;
        public int[] HistogramGreen;
        public int[] HistogramBlue;
        public int[] HistogramUsredniony;
        public int MaxPixels;

        public Show_Histogram(BitmapSource imageToedit)
        {
            InitializeComponent();
            image_his = imageToedit;
           
            Histogram_Usredniony.Fill = Brushes.Black;
            Histogram_Czerwony.Fill = Brushes.Red;
            Histogram_Zielony.Fill = Brushes.Green;
            Histogram_Niebieski.Fill = Brushes.Blue;
           

            createHistograms();
        }

        private void createHistograms()
        {
            FillHistogram(image_his);
            Histogram_Usredniony.Points = Points(HistogramUsredniony);
            MaxUsredniony.Content = HistogramUsredniony.Max();

            Histogram_Czerwony.Points = Points(HistogramRed);
            MaxCzerwonyy.Content = HistogramRed.Max();

            Histogram_Zielony.Points = Points(HistogramGreen);
            MaxZielony.Content = HistogramGreen.Max();

            Histogram_Niebieski.Points = Points(HistogramBlue);
            MaxNiebieski.Content = HistogramBlue.Max();

        }
        public void FillHistogram(BitmapSource imgEdit)
        {
            HistogramRed = new int[256];
            HistogramGreen = new int[256];
            HistogramBlue = new int[256];
            HistogramUsredniony = new int[256];
            MaxPixels = imgEdit.PixelWidth * imgEdit.PixelHeight;
            var stride = imgEdit.PixelWidth * ((imgEdit.Format.BitsPerPixel + 7) / 8);
            var pixels = new byte[stride * imgEdit.PixelHeight];
            imgEdit.CopyPixels(pixels, stride, 0);
            var j = 0;
            for (var i = 0; i < pixels.Length / 4; i++)
            {
                var r = pixels[j + 2];
                var g = pixels[j + 1];
                var b = pixels[j];

                HistogramRed[r]++;
                HistogramGreen[g]++;
                HistogramBlue[b]++;

                var temp = (r + g + b) / 3;
                HistogramUsredniony[temp]++;

                j += 4;
            }
        }
        private void Bright_Click(object sender, RoutedEventArgs e)
        {
            var lut = new int[256];
            var max = 0;

            for (var i = 255; i >= 0; i--)
                if (HistogramUsredniony[i] > 0)
                {
                    max = i;
                    break;
                }


            for (int i = 0; i < 256; i++)
            {         
                double licznik = Math.Log(1 + i);
                double mianownik = Math.Log(1 + max);
                lut[i] = (int)Math.Round(255.0 * (licznik / mianownik), 0, MidpointRounding.AwayFromZero);
                if (lut[i] > 255)
                {
                    lut[i] = 255;
                }
                if (lut[i] < 0)
                {
                    lut[i] = 0;
                }
            }

            var copybitmap = new WriteableBitmap(image_his);
            var stride = copybitmap.PixelWidth * ((copybitmap.Format.BitsPerPixel + 7) / 8);
            var pixels = new byte[stride * copybitmap.PixelHeight];
            copybitmap.CopyPixels(pixels, stride, 0);
            var j = 0;

            for (var i = 0; i < pixels.Length / 4; i++)
            {
                var r = pixels[j + 2];
                var g = pixels[j + 1];
                var b = pixels[j];

                pixels[j + 2] = (byte)lut[r];
                pixels[j + 1] = (byte)lut[g];
                pixels[j] = (byte)lut[b];

                j += 4;
            }

            var rect = new Int32Rect(0, 0, copybitmap.PixelWidth, copybitmap.PixelHeight);
            copybitmap.WritePixels(rect, pixels, stride, 0);
            image_his = copybitmap;         
            ((MainWindow)System.Windows.Application.Current.MainWindow).EditImage.Source = image_his;
            createHistograms();
        }

        private void Dark_Click(object sender, RoutedEventArgs e)
        {
            var lut = new int[256];
            var max = 0;

            for (var i = 255; i >= 0; i--)
                if (HistogramUsredniony[i] > 0)
                {
                    max = i;
                    break;
                }
            for (int i = 0; i < 256; i++)
            {                
                lut[i] = (int)Math.Round(255.0 * (Math.Pow((double)i / max, 2.0)), 0, MidpointRounding.AwayFromZero);

                if (lut[i] > 255)
                {
                    lut[i] = 255;
                }
                if (lut[i] < 0)
                {
                    lut[i] = 0;
                }
            }

            var copybitmap = new WriteableBitmap(image_his);      
            var stride = copybitmap.PixelWidth * ((copybitmap.Format.BitsPerPixel + 7) / 8);
            var pixels = new byte[stride * copybitmap.PixelHeight];
            copybitmap.CopyPixels(pixels, stride, 0);
            var j = 0;
            for (var i = 0; i < pixels.Length / 4; i++)
            {
                var r = pixels[j + 2];
                var g = pixels[j + 1];
                var b = pixels[j];
                pixels[j + 2] = (byte)lut[r];
                pixels[j + 1] = (byte)lut[g];
                pixels[j] = (byte)lut[b];
                j += 4;
            }
            var rect = new Int32Rect(0, 0, copybitmap.PixelWidth, copybitmap.PixelHeight);
            copybitmap.WritePixels(rect, pixels, stride, 0);
            image_his = copybitmap;        
            ((MainWindow)System.Windows.Application.Current.MainWindow).EditImage.Source = image_his;
            createHistograms();
        }


        private void Equal_Click(object sender, RoutedEventArgs e)
        {
            var dystR = GetDyst(HistogramRed);
            var dystG = GetDyst(HistogramGreen);
            var dystB = GetDyst(HistogramBlue);

            var lutR = LutEqual(dystR, HistogramRed.Length);
            var lutG = LutEqual(dystG, HistogramGreen.Length);
            var lutB = LutEqual(dystB, HistogramBlue.Length);

            BitmapSource bmp = EqualizeHistogram(lutR, lutG, lutB, image_his);
            ((MainWindow)Application.Current.MainWindow).EditImage.Source = image_his = bmp;
            createHistograms();
        }
        public static PointCollection Points(int[] values)
        {
            var max = values.Max();
            var points = new PointCollection();
            points.Add(new Point(0, max));
            for (var i = 0; i < values.Length; i++)
            {
                points.Add(new Point(i, max - values[i]));
            }
            points.Add(new Point(values.Length - 1, max));
            return points;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).ConfirmHist(image_his);
            this.Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Cancel_His(image_his);
            this.Close();
        }

        private void Extension_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Cancel_His(image_his);
            this.Close();
        }

        private void Undo_Click(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).Cancel_His(image_his_ext);
            image_his = image_his_ext;
            createHistograms();
        }

        public int Validation(int min, int max)
        {        
            if (max <= 255 && min < max)
            {
                return 1;
            }
            return 0;
        }
        private void Extension_Usredniony(object sender, RoutedEventArgs e)
        {
            if(Validation(int.Parse(UsrednionyBoxMin.Text), int.Parse(UsrednionyBoxMax.Text)) == 1) {            
            image_his_ext = image_his;
            var lut = GetLutStretching(int.Parse(UsrednionyBoxMin.Text), int.Parse(UsrednionyBoxMax.Text));
            var bitmap = new WriteableBitmap(image_his);
            var stride = bitmap.PixelWidth * ((bitmap.Format.BitsPerPixel + 7) / 8);
            var pixels = new byte[stride * bitmap.PixelHeight];
            bitmap.CopyPixels(pixels, stride, 0);

            var j = 0;

            for (var i = 0; i < pixels.Length / 4; i++)
            {
                var r = pixels[j + 2];
                var g = pixels[j + 1];
                var b = pixels[j];      
                        pixels[j + 2] = (byte)lut[r];
                        pixels[j + 1] = (byte)lut[g];
                        pixels[j] = (byte)lut[b];            
                j += 4;
            }

            var rect = new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight);
            bitmap.WritePixels(rect, pixels, stride, 0);
                ((MainWindow)Application.Current.MainWindow).EditImage.Source = bitmap;
                image_his = bitmap;
                createHistograms();
            }
            else
            {
                MessageBox.Show("Wprowadź poprawne wartości");
            }      
        }

        private void Extension_Czerwony(object sender, RoutedEventArgs e)
        {
            if (Validation(int.Parse(CzerwonyBoxMin.Text), int.Parse(CzerwonyBoxMax.Text)) == 1)
            {
                image_his_ext = image_his;
                var lut = GetLutStretching(int.Parse(CzerwonyBoxMin.Text), int.Parse(CzerwonyBoxMax.Text));
                var bitmap = new WriteableBitmap(image_his);
                var stride = bitmap.PixelWidth * ((bitmap.Format.BitsPerPixel + 7) / 8);
                var pixels = new byte[stride * bitmap.PixelHeight];
                bitmap.CopyPixels(pixels, stride, 0);
                var j = 0;
                for (var i = 0; i < pixels.Length / 4; i++)
                {
                    var x = pixels[j + 2];
                    pixels[j + 2] = (byte)lut[x];
                    j += 4;
                }
                var rect = new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight);
                bitmap.WritePixels(rect, pixels, stride, 0);
                ((MainWindow)Application.Current.MainWindow).EditImage.Source = bitmap;
                image_his = bitmap;
                createHistograms();
            }
            else
            {
                MessageBox.Show("Wprowadź poprawne wartości");
            }
          
        }
        private void Extension_Zielony(object sender, RoutedEventArgs e)
        {
            if (Validation(int.Parse(ZielonyBoxMin.Text), int.Parse(ZielonyBoxMax.Text)) == 1)
            {
                image_his_ext = image_his;
                var lut = GetLutStretching(int.Parse(ZielonyBoxMin.Text), int.Parse(ZielonyBoxMax.Text));
                var bitmap = new WriteableBitmap(image_his);
                var stride = bitmap.PixelWidth * ((bitmap.Format.BitsPerPixel + 7) / 8);
                var pixels = new byte[stride * bitmap.PixelHeight];
                bitmap.CopyPixels(pixels, stride, 0);
                var j = 0;
                for (var i = 0; i < pixels.Length / 4; i++)
                {
                    var x = pixels[j + 1];
                    pixels[j + 1] = (byte)lut[x];
                    j += 4;
                }
                var rect = new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight);
                bitmap.WritePixels(rect, pixels, stride, 0);
                ((MainWindow)Application.Current.MainWindow).EditImage.Source = bitmap;
                image_his = bitmap;
                createHistograms();
            }
            else
            {
                MessageBox.Show("Wprowadź poprawne wartości");
            }
        }

        private void Extension_Niebieski(object sender, RoutedEventArgs e)
        {
            if (Validation(int.Parse(NiebieskiBoxMin.Text), int.Parse(NiebieskiBoxMax.Text)) == 1)
            {
                image_his_ext = image_his;
                var lut = GetLutStretching(int.Parse(NiebieskiBoxMin.Text), int.Parse(NiebieskiBoxMax.Text));
                var bitmap = new WriteableBitmap(image_his);
                var stride = bitmap.PixelWidth * ((bitmap.Format.BitsPerPixel + 7) / 8);
                var pixels = new byte[stride * bitmap.PixelHeight];
                bitmap.CopyPixels(pixels, stride, 0);

                var j = 0;
                for (var i = 0; i < pixels.Length / 4; i++)
                {
                    var x = pixels[j];
                    pixels[j] = (byte)lut[x];
                    j += 4;
                }
                var rect = new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight);
                bitmap.WritePixels(rect, pixels, stride, 0);
                ((MainWindow)Application.Current.MainWindow).EditImage.Source = bitmap;
                image_his = bitmap;
                createHistograms();
            }
            else
            {
                MessageBox.Show("Wprowadź poprawne wartości");
            }

        }

        public double[] GetDyst(int[] histogram)
        {
            double suma = 0;
            var dyst = new double[histogram.Length];
            for (var i = 0; i < histogram.Length; i++)
            {
                suma += histogram[i];
                dyst[i] = suma / MaxPixels;
            }
            return dyst;
        }

        public int[] GetLutStretching(double min, double max)
        {
            var lut = new int[256];
            for (var i = 0; i < 256; i++)
            {
                var value = (255.0 / (max - min)) * (i - min);
                if (value > 255)
                {
                    lut[i] = 255;
                }
                else if (value < 0)
                {
                    lut[i] = 0;
                }
                else
                {
                    lut[i] = (int)Math.Round(value, 0, MidpointRounding.AwayFromZero);
                }
            }
            return lut;
        }

        public int[] LutEqual(double[] dyst, int max)
        {
            var i = 0;
            while (dyst[i] == 0)
            {
                i++;
            }
            var tmp = dyst[i];
            var lut = new int[256];
            for (i = 0; i < 256; i++)
            {
                lut[i] = (int)((dyst[i] - tmp) / (1 - tmp) * (max - 1));
                if (lut[i] > 255)
                {
                    lut[i] = 255;
                }
                if (lut[i] < 0)
                {
                    lut[i] = 0;
                }
            }
            return lut;
        }
        public BitmapSource EqualizeHistogram(int[] lutR, int[] lutG, int[] lutB, BitmapSource imgEdit)
        {
            WriteableBitmap bitmap = new WriteableBitmap(imgEdit);
            var stride = bitmap.PixelWidth * ((bitmap.Format.BitsPerPixel + 7) / 8);
            var pixels = new byte[stride * bitmap.PixelHeight];
            bitmap.CopyPixels(pixels, stride, 0);
            var j = 0;
            for (var i = 0; i < pixels.Length / 4; i++)
            {
                var r = pixels[j + 2];
                var g = pixels[j + 1];
                var b = pixels[j];

                pixels[j + 2] = (byte)lutR[r];
                pixels[j + 1] = (byte)lutG[g];
                pixels[j] = (byte)lutB[b];

                j += 4;
            }
            var rect = new Int32Rect(0, 0, bitmap.PixelWidth, bitmap.PixelHeight);
            bitmap.WritePixels(rect, pixels, stride, 0);
            return bitmap;
        }

    }
}
