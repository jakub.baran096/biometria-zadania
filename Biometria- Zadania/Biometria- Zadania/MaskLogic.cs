﻿namespace Biometria__Zadania
{
    class MaskLogic
    {

        public static int[,] RozmycieTabela =
        {
            {1, 1, 1},
            {1, 1, 1},
            {1, 1, 1}
        };

        public static int[,] PrewittPoziomoTabela =
        {
            {-1, -1, -1},
            {0, 0, 0},
            {1, 1, 1}
        };

        public static int[,] PrewittPionowoTabela =
        {
            {-1, -1, -1},
            {0, 0, 0},
            {1, 1, 1}
        };

        public static int[,] SobelPionowoTabela =
        {
            {-1, 0, 1},
            {-2, 0, 2},
            {-1, 0, 1}
        };

        public static int[,] SobelPoziomoTabela =
        {
            {1, 2, 1},
            {0, 0, 0},
            {-1, -2, -1}
        };

        public static int[,] LaplacePoziomoTabela =
        {
            {0, -1, 0},
            {0, 2, 0},
            {0, -1, 0}
        };

        public static int[,] LaplacePionowo =
        {
            {0, 0, 0},
            {-1, 2, -1},
            {0, 0, 0}
        };

        public static int[,] LaplaceUkosny =
        {
            {-1, 0, -1},
            {0, 4, 0},
            {-1, 0, -1}
        };

        public static int[,] WykrywanieWschod =
        {
            {-1, 1, 1},
            {-1, -2, 1},
            {-1, 1, 1}
        };

        public static int[,] WykrywaniePoludnie =
        {
            {-1, -1, 1},
            {-1, -2, 1},
            {1, 1, 1}
        };

        public static int[,] WykrywanieZachod =
        {
            {1, 1, -1},
            {1, -2, -1},
            {1, 1, -1}
        };

        public static int[,] WykrywaniePolnoc =
        {
            {1, 1, 1},
            {1, -2, 1},
            {-1, -1, -1}
        };

        public static int[,] WykrywaniePoludniowyWschod =
        {
            {-1, -1, 1},
            {-1, -2, 1},
            {1, -1, -1}
        };

        public static int[,] WykrywaniePoludniowyZachod =
        {
            {1, -1, -1},
            {1, -2, -1},
            {1, 1, 1}
        };

        public static int[,] WykrywaniePolnocnyZachod =
        {
            {1, 1, 1},
            {1, -2, -1},
            {1, -1, -1}
        };

        public static int[,] WykrywaniePolnocnyWschod =
        {
            {1, 1, 1},
            {-1, -2, 1},
            {-1, -1, 1}
        };

    }
}
