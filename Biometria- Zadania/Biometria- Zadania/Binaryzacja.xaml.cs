﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows;
using System.Windows.Media.Imaging;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

namespace Biometria__Zadania
{
    /// <summary>
    /// Logika interakcji dla klasy Binaryzacja.xaml
    /// </summary>
    /// 
    public partial class Binaryzacja : Window
    {

        public BitmapSource editImageBit_Binaryzacja; // Obrazek edytowany
        public BitmapSource editImageBit_Cancel;
        public Binaryzacja(BitmapSource editImageBit)
        {
            editImageBit_Binaryzacja = editImageBit;
            editImageBit_Cancel = editImageBit;

            InitializeComponent();
        }

        private void Binaryzacja_Automatyczna(object sender, RoutedEventArgs e)
        {
            var sill = GetWariancja(editImageBit_Binaryzacja);
            var result = ManualBinarisation(editImageBit_Binaryzacja, sill);
            ((MainWindow)Application.Current.MainWindow).editImageBit = result;
            ((MainWindow)Application.Current.MainWindow).EditImage.Source = result;
        }

        private void Binaryzacja_Reczna(object sender, RoutedEventArgs e)
        {
            var sill = int.Parse(Binaryzacja_Reczna_TextBox.Text);
            var result = ManualBinarisation(editImageBit_Binaryzacja, sill);
            ((MainWindow)Application.Current.MainWindow).editImageBit = result;
            ((MainWindow)Application.Current.MainWindow).EditImage.Source = result;
        }

        public static WriteableBitmap ManualBinarisation(BitmapSource image, int treshhold)
        {
            var bitmap = new WriteableBitmap(image);
            var width = bitmap.PixelWidth;
            var height = bitmap.PixelHeight;
            var stride = width * ((bitmap.Format.BitsPerPixel + 7) / 8);
            var arraySize = stride * height;
            var pixels = new byte[arraySize];
         
            bitmap.CopyPixels(pixels, stride, 0);

            var j = 0;
  
            for (var i = 0; i < pixels.Length / 4; i++)
            {        
                var oldPixelValue = pixels[j];     
                var value = oldPixelValue <= treshhold ? 0 : 255;
                pixels[j + 2] = (byte)value;
                pixels[j + 1] = (byte)value;
                pixels[j] = (byte)value;
                j += 4;
            }

            var rect = new Int32Rect(0, 0, width, height);
            bitmap.WritePixels(rect, pixels, stride, 0);

            return bitmap;
        }

        public static int GetWariancja(BitmapSource image)
        {
            var pixelsTab = GetPixelsValuesArray(image);
            var bgWeight = new double[256];
            var bgMean = new double[256];
            var fgWeight = new double[256];
            var fgMean = new double[256];

            var max = image.PixelWidth * image.PixelHeight;
            for (var i = 0; i < 256; i++)
            {
                bgWeight[i] = GetWeight(pixelsTab, max, 0, i);
                bgMean[i] = GetAverage(pixelsTab, 0, i);

                fgWeight[i] = GetWeight(pixelsTab, max, i + 1, 256);
                fgMean[i] = GetAverage(pixelsTab, i + 1, 256);
            }

            double maximum = 0;
            var pozycja = 0;
            for (var j = 0; j < 256; j++)
            {
                var wynik = bgWeight[j] * fgWeight[j] * (bgMean[j] - fgMean[j]) * (bgMean[j] - fgMean[j]);
                if (wynik > maximum)
                {
                    maximum = wynik;
                    pozycja = j;
                }
            }
            return pozycja;
        }

        private static double GetWeight(int[] pixelsValues, int maxPixels, int start, int end)
        {
            double suma = 0;
            for (var i = start; i < end; i++)
                suma += pixelsValues[i];
            return suma / maxPixels;
        }

        private static double GetAverage(int[] pixelsValues, int start, int end)
        {
            double max = 0;
            double suma = 0;
            for (var i = start; i < end; i++)
            {
                suma += pixelsValues[i] * i;
                max += pixelsValues[i];
            }
            return suma / max;
        }

        private static int[] GetPixelsValuesArray(BitmapSource image)
        {
            var tab = new int[256];
            var bitmap = new WriteableBitmap(image);
            var width = bitmap.PixelWidth;
            var height = bitmap.PixelHeight;
            var stride = width * ((bitmap.Format.BitsPerPixel + 7) / 8);
            var arraySize = stride * height;
            var pixels = new byte[arraySize];      
            bitmap.CopyPixels(pixels, stride, 0);
            var j = 0;   
            for (var i = 0; i < pixels.Length / 4; i++)
            {           
                var value = pixels[j + 2];
                tab[value]++;
                j += 4;
            }

            return tab;
        }

        private void Niblacka_Start(object sender, RoutedEventArgs e)
        {
            int _size = int.Parse(Niblacka_Rozmiar.Text);
            double _parameter = double.Parse(Niblacka_Progowanie.Text);
            ((MainWindow)Application.Current.MainWindow).EditImage.Source = Niblack(_parameter, _size);
        }
       
        private static Bitmap GetBitmap(BitmapSource source)
        {
            var bmp = new Bitmap(
                source.PixelWidth,
                source.PixelHeight,
                PixelFormat.Format32bppPArgb);
            var data = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.WriteOnly,
                PixelFormat.Format32bppPArgb);
            source.CopyPixels(
                Int32Rect.Empty,
                data.Scan0,
                data.Height * data.Stride,
                data.Stride);
            bmp.UnlockBits(data);
            return bmp;
        }


        public WriteableBitmap Niblack(double k, int windowSize)
        {
            MessageBox.Show("Rozpoczęto!");
            var img = editImageBit_Binaryzacja;
            var image1 = GetBitmap(img);
            var image2 = GetBitmap(img);

            var tresholdList1 = new List<int>();
            var tresholdList2 = new List<int>();

            var r = (windowSize - 1) / 2;
            var width = image1.Width;
            var imgHeight = image1.Height;
            var imgSize = new Size(width, imgHeight);

            var pierwszaWysokosc = imgHeight / 2;

            for (var y = 0; y < pierwszaWysokosc; y++)
                for (var x = 0; x < width; x++)
                {
                    var prog = GetThreshold(imgSize, image1, r, k, windowSize, x, y);
                    tresholdList1.Add(prog);
                }

            for (var y = pierwszaWysokosc; y <= imgHeight; y++)
                for (var x = 0; x < width; x++)
                {
                    var prog = GetThreshold(imgSize, image2, r, k, windowSize, x, y);
                    tresholdList2.Add(prog);
                }


            image1.Dispose();
            image2.Dispose();

  
            tresholdList1.AddRange(tresholdList2);

            var bitmap = new WriteableBitmap(img);
            var widthNew = bitmap.PixelWidth;
            var height = bitmap.PixelHeight;
            var stride = widthNew * ((bitmap.Format.BitsPerPixel + 7) / 8);
            var arraySize = stride * height;
            var pixels = new byte[arraySize];
         
            bitmap.CopyPixels(pixels, stride, 0);

            var j = 0;        
            for (var i = 0; i < pixels.Length / 4; i++)
            {
               
                var oldPixelValue = pixels[j];
                int value;
                
                if (oldPixelValue <= tresholdList1[i])
                    value = 0;
                else
                    value = 255;

                pixels[j + 2] = (byte)value;
                pixels[j + 1] = (byte)value;
                pixels[j] = (byte)value;

                j += 4;
            }

            var rect = new Int32Rect(0, 0, width, height);
            bitmap.WritePixels(rect, pixels, stride, 0);
            MessageBox.Show("Zakonczono");
            return bitmap;
        }

        private static int GetThreshold(Size dimensions, Bitmap image, int r, double k, int windowSize, int x, int y)
        {
            
            var x1 = x - r;
            var x2 = x + r;
            var y1 = y - r;
            var y2 = y + r;

            if (x1 < 0) x1 = 0;
            if (x2 > dimensions.Width) x2 = (int)dimensions.Width;
            if (y1 < 0) y1 = 0;
            if (y2 > dimensions.Height) y2 = (int)dimensions.Height;

            var window = new Bitmap(x2 - x1, y2 - y1);
            using (var g = Graphics.FromImage(window))
            {
                var rect = new Rectangle(x1, y1, x2 - x1, y2 - y1);
                g.DrawImage(image, 0, 0, rect, GraphicsUnit.Pixel);
            }
            var histogram = GetHistogramUsredniony(window);
            if (window.Width * window.Height < windowSize * windowSize)
            {
                var diff = windowSize * windowSize - window.Width * window.Height;
                histogram[0] += diff;
            }
            var average = GetAverage(histogram, 0, histogram.Length);
            var variation = GetVariation(histogram, average);
            var threshold = (int)Math.Round(average + k * variation, 0, MidpointRounding.AwayFromZero);
            window.Dispose();
            return threshold;
        }


        public static int[] GetHistogramUsredniony(Bitmap obrazek)
        {
            var bmp = new Bitmap(obrazek);
            var histogram = new int[256];
           

            unsafe
            {
                var bitmapData = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite,
                    bmp.PixelFormat);
                var bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
                var heightInPixels = bitmapData.Height;
                var widthInBytes = bitmapData.Width * bytesPerPixel;
                var ptrFirstPixel = (byte*)bitmapData.Scan0;

                for (var y = 0; y < heightInPixels; y++)
                {
                    var currentLine = ptrFirstPixel + y * bitmapData.Stride;
                    for (var x = 0; x < widthInBytes; x = x + bytesPerPixel)
                    {
                        int oldBlue = currentLine[x];
                        int oldGreen = currentLine[x + 1];
                        int oldRed = currentLine[x + 2];

                        //usredniony
                        var tmp = 0;
                        tmp += oldRed;
                        tmp += oldGreen;
                        tmp += oldBlue;
                        tmp = tmp / 3;
                        histogram[tmp]++;
                    }
                }
                bmp.UnlockBits(bitmapData);
            }
            return histogram;
        }
        private static double GetVariation(int[] histogram, double srednia)
        {
            double temp = 0;
            double max = 0;
            for (var k = 0; k < histogram.Length; k++)
            {
                temp += histogram[k] * Math.Pow(k - srednia, 2.0);
                max += histogram[k];
            }
            return Math.Sqrt(temp / max);
        }

        private void Binaryzacja_Cofnij(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).EditImage.Source = editImageBit_Cancel;
            editImageBit_Binaryzacja = editImageBit_Cancel;
        }
        private void Binaryzacja_Akceptuj(object sender, RoutedEventArgs e)
        {
            // ((MainWindow)Application.Current.MainWindow).editImageBit = editImageBit_Binaryzacja;
            this.Close();
        }
        private void Binaryzacja_Zamknij(object sender, RoutedEventArgs e)
        {
            ((MainWindow)Application.Current.MainWindow).EditImage.Source = editImageBit_Cancel;
            ((MainWindow)Application.Current.MainWindow).editImageBit = editImageBit_Cancel;
            this.Close();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            pbStatus.Value = e.ProgressPercentage;
        }

    }
}
