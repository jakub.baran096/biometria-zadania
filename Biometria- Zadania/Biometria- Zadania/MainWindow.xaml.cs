﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Biometria__Zadania
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BitmapSource sourcelImageBit; //Obrazek orginalny
        public BitmapSource editImageBit; // Obrazek edytowany
        public BitmapSource editImageBit_beforeHis; // Obrazek edytowany
        private TransformGroup transferGroup = new TransformGroup(); // Zmienna do transformacji (skalowanie) 
        private double zoomValue = 100;  // Zmienna okreslajacy wartość przyblizenia
        private double editX = 0; // Pozycje obrazka
        private double editY = 0;

        private Filtry filtry;
        public MainWindow()
        {
            InitializeComponent();
            ScaleTransform scale = new ScaleTransform();  //skalowanie obrazu 
            TranslateTransform translate = new TranslateTransform(); // przesuwanie obrazu
            filtry = new Filtry();
            transferGroup.Children.Add(scale);
            transferGroup.Children.Add(translate);
            zoomValueLabel.Content = zoomValue.ToString() + "%";

        }

        #region Zadanie 1, Wczytanie, Zapis, Zoom, RGB
        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dl = new OpenFileDialog();

            dl.DefaultExt = ".png";
            dl.Filter = "Format|*.jpg;*.jpeg;*.png;*.bmp;*.gif;*.tiff|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png |" + "BMP (*.bmp)|*.bmp|" + "GIF (*.gif)|*.gif|" + "TIFF (*.tiff)|*.tiff";
            dl.ShowDialog();
            string name = dl.FileName;
            if (dl.CheckPathExists && name != "")
            {
                sourcelImageBit = new BitmapImage(new Uri(name));
                if (sourcelImageBit.Format != PixelFormats.Bgra32)
                {
                    sourcelImageBit = new FormatConvertedBitmap(sourcelImageBit, PixelFormats.Bgra32, null, 0);
                }
                editImageBit = sourcelImageBit.Clone();
                SourceImage.Source = sourcelImageBit;
                EditImage.Source = editImageBit;
                MessageBox.Show("Załadowano obraz");
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (editImageBit == null)
            {
                MessageBox.Show("Nie ma co zapisać");
                return;
            }

            SaveFileDialog dig = new SaveFileDialog();
            dig.Title = "Zapisz zdjęcie";
            dig.Filter = "Format|*.jpg;*.jpeg;*.png;*.bmp;*.gif;*.tiff|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png |" + "BMP (*.bmp)|*.bmp|" + "GIF (*.gif)|*.gif|" + "TIFF (*.tiff)|*.tiff";

            string fileName = string.Empty;
            string extension = string.Empty;

            if (dig.ShowDialog() == true)
            {
                fileName = dig.FileName;
                extension = fileName.Substring(fileName.LastIndexOf('.') + 1);
            }
            else
            {
                return;
            }

            using (FileStream stream = new FileStream(fileName, FileMode.Create))
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    BitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(editImageBit));
                    encoder.Save(memory);
                    memory.Position = 0;

                    switch (extension)
                    {
                        case "png":
                            PngBitmapEncoder pngEncoder = new PngBitmapEncoder();
                            pngEncoder.Frames.Add(BitmapFrame.Create(memory));
                            pngEncoder.Save(stream);
                            break;

                        case "jpg":
                            JpegBitmapEncoder jpegEncoder = new JpegBitmapEncoder();
                            jpegEncoder.Frames.Add(BitmapFrame.Create(memory));
                            jpegEncoder.Save(stream);
                            break;

                        case "bmp":
                            BmpBitmapEncoder bitmapEncoder = new BmpBitmapEncoder();
                            bitmapEncoder.Frames.Add(BitmapFrame.Create(memory));
                            bitmapEncoder.Save(stream);
                            break;

                        case "tiff":
                            TiffBitmapEncoder tifmapEncoder = new TiffBitmapEncoder();
                            tifmapEncoder.Frames.Add(BitmapFrame.Create(memory));
                            tifmapEncoder.Save(stream);
                            break;
                    }
                }
            }

        }

        private void SetRGB_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

            var x = e.GetPosition(EditImage).X * (editImageBit.PixelWidth / EditImage.ActualWidth);
            var y = e.GetPosition(EditImage).Y * (editImageBit.PixelHeight / EditImage.ActualHeight);

            int stride = editImageBit.PixelWidth * 4;
            int size = editImageBit.PixelHeight * stride;
            byte[] pixels = new byte[size];
            editImageBit.CopyPixels(pixels, stride, 0);

            int index = (int)y * stride + (int)x * 4;   //BGRA
            byte blue = pixels[index];
            byte green = pixels[index + 1];
            byte red = pixels[index + 2];
            byte alpha = pixels[index + 3];

            RGBSelector newWindow = new RGBSelector(System.Drawing.Color.FromArgb(alpha, red, green, blue));
            newWindow.ShowDialog();
            pixels[index] = newWindow.myColor.B;
            pixels[index + 1] = newWindow.myColor.G;
            pixels[index + 2] = newWindow.myColor.R;
            pixels[index + 3] = newWindow.myColor.A;

            WriteableBitmap wb = new WriteableBitmap(editImageBit);
            wb.WritePixels(new Int32Rect(0, 0, editImageBit.PixelWidth, editImageBit.PixelHeight), pixels, stride, 0);
            editImageBit = wb;
            EditImage.Source = editImageBit;

        }

        private void MoveImage_MouseMove(object sender, MouseEventArgs e)
        {
            Image image = sender as Image;
            if (image != null && e.LeftButton == MouseButtonState.Pressed)
            {
                TranslateTransform tt = new TranslateTransform(e.GetPosition(BorderCopy).X - (editX * zoomValue / 100), e.GetPosition(BorderCopy).Y - (editY * zoomValue / 100));
                transferGroup.Children[1] = tt;
                EditImage.RenderTransform = transferGroup;
                SourceImage.RenderTransform = transferGroup;
            }
            RGBValues.Text = GetEditedRGB(e);
        }

        public string GetEditedRGB(MouseEventArgs e)
        {
            double x = e.GetPosition(EditImage).X * (editImageBit.PixelWidth / EditImage.ActualWidth);
            double y = e.GetPosition(EditImage).Y * (editImageBit.PixelHeight / EditImage.ActualHeight);

            int size_1 = editImageBit.PixelWidth * 4;
            int size_2 = editImageBit.PixelHeight * size_1;
            byte[] pixels = new byte[size_2];

            editImageBit.CopyPixels(pixels, size_1, 0);

            int index = (int)y * size_1 + (int)x * 4;   //BGRA
            byte blue = pixels[index];
            byte green = pixels[index + 1];
            byte red = pixels[index + 2];
            try
            {
                return ("RGB: (" + red.ToString() + "," + green.ToString() + "," + blue.ToString() + ")");
            }
            finally
            {
               
            }
           
        }
        private void SetCoordinates_MouseDown(object sender, MouseButtonEventArgs e)
        {
            editX = e.GetPosition(EditImage).X;
            editY = e.GetPosition(EditImage).Y;
        }

        private void Zoom_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                zoomValue += 10;
            }
            else
            {
                zoomValue -= 10;
            }
            ScaleTransform flipTrans = new ScaleTransform();
            flipTrans.ScaleX = (zoomValue / 100);
            flipTrans.ScaleY = zoomValue / 100;
            transferGroup.Children[0] = flipTrans;
            SourceImage.RenderTransform = transferGroup;
            EditImage.RenderTransform = transferGroup;
            zoomValueLabel.Content = zoomValue.ToString() + "%";
        }

        private void ZoomIN_Click(object sender, RoutedEventArgs e)
        {
            ScaleTransform flipTrans = new ScaleTransform();
            flipTrans.ScaleX = ((zoomValue = zoomValue + 10) / 100);
            flipTrans.ScaleY = zoomValue / 100;
            transferGroup.Children[0] = flipTrans;
            SourceImage.RenderTransform = transferGroup;
            EditImage.RenderTransform = transferGroup;
            zoomValueLabel.Content = zoomValue.ToString() + "%";
        }

        private void ZoomOUT_Click(object sender, RoutedEventArgs e)
        {
            ScaleTransform flipTrans = new ScaleTransform();
            flipTrans.ScaleX = ((zoomValue = zoomValue - 10) / 100);
            flipTrans.ScaleY = zoomValue / 100;
            transferGroup.Children[0] = flipTrans;
            SourceImage.RenderTransform = transferGroup;
            EditImage.RenderTransform = transferGroup;
            zoomValueLabel.Content = zoomValue.ToString() + "%";
        }


        //Prawy panel - przyciski do cofania/resetowania
        private void BackToSource_Click(object sender, RoutedEventArgs e)
        {
            editImageBit = sourcelImageBit;
            EditImage.Source = sourcelImageBit;
            MessageBox.Show("Przywrócono obrazek");
        }

        private void ResetPosition_Click(object sender, RoutedEventArgs e)
        {
            TranslateTransform tt = new TranslateTransform(0, 0);
            transferGroup.Children[1] = tt;
            EditImage.RenderTransform = transferGroup;
            SourceImage.RenderTransform = transferGroup;
        }

        private void ResetScale_Click(object sender, RoutedEventArgs e)
        {
            ScaleTransform flipTrans = new ScaleTransform();
            zoomValue = 100;
            flipTrans.ScaleX = (zoomValue / 100);
            flipTrans.ScaleY = zoomValue / 100;
            transferGroup.Children[0] = flipTrans;
            SourceImage.RenderTransform = transferGroup;
            EditImage.RenderTransform = transferGroup;
            zoomValueLabel.Content = zoomValue.ToString() + "%";
        }

        public void Quit_App(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
        #endregion

        #region Zadanie 2 Histogram
        private void Open_Histogram(object sender, RoutedEventArgs e)
        {

            if (editImageBit != null)
            {
                editImageBit_beforeHis = editImageBit;
                Show_Histogram histogram = new Show_Histogram(editImageBit);
                histogram.ShowDialog();
            }
            else
            {
                MessageBox.Show("Najpierw wczytaj zdjęcie");
            }

        }
        //Jeżeli anuluje sie zmiany w histogramie 
        public void ConfirmHist(BitmapSource img)
        {
            editImageBit = img;
        }
        public void Cancel_His(BitmapSource img)
        {
            editImageBit = editImageBit_beforeHis;
            EditImage.Source = editImageBit;
        }
        #endregion


        #region Zadanie 3 
        private void Open_Binaryzacja(object sender, RoutedEventArgs e)
        {

            if (editImageBit != null)
            {
                editImageBit_beforeHis = editImageBit;
                Binaryzacja histogram = new Binaryzacja(editImageBit);
                histogram.ShowDialog();
            }
            else
            {
                MessageBox.Show("Najpierw wczytaj zdjęcie");
            }

        }

        #endregion


        #region Zadanie 4 

        private void Mask_Click_Show(object sender, RoutedEventArgs e)
        {
            var Mask = new MaskWindow();
            Mask.Show();
        }

        private void Rozmycie_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.RozmycieTabela, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Prewitt_Poziomo_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.PrewittPoziomoTabela, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Prewitt_Pionowo_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.PrewittPionowoTabela, 3);
            EditImage.Source = img;
            editImageBit = img;

        }

        private void Sobel_Poziomo_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.SobelPoziomoTabela, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Sobel_Pionowo_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.SobelPionowoTabela, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Laplace_Pionowo_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.LaplacePionowo, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Laplace_Poziomo_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.LaplacePoziomoTabela, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Laplace_Ukosny_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.LaplaceUkosny, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Wykrywanie_Wschod_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywanieWschod, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Wykrywanie_PoludniowyWschod_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywaniePoludniowyWschod, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Wykrywanie_Poludnie_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywaniePoludnie, 3);
            EditImage.Source = img;
            editImageBit = img;
        }
        private void Wykrywanie_PoludniowyZachod_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywaniePoludniowyZachod, 3);
            EditImage.Source = img;
            editImageBit = img;
        }
        private void Wykrywanie_Zachod_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywanieZachod, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Wykrywanie_PolnocnyZachod_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywaniePoludniowyZachod, 3);
            EditImage.Source = img;
            editImageBit = img;
        }
        private void Wykrywanie_Polnoc_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywaniePolnoc, 3);
            EditImage.Source = img;
            editImageBit = img;
        }
        private void Wykrywanie_PolnocnyWschod_Click(object sender, RoutedEventArgs e)
        {
            var img = Filtry.ManualMask(MaskLogic.WykrywaniePolnocnyWschod, 3);
            EditImage.Source = img;
            editImageBit = img;
        }

        private void Kuwahara_5(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Zostaniesz poinformowaniu po zakończeniu procesu");
            var img = Filtry.Kuwahara(5);
            EditImage.Source = img;
            editImageBit = img;
            MessageBox.Show("Zakończono!");
        }

        private void Medianowy_3_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Zostaniesz poinformowaniu po zakończeniu procesu");
            var img = Filtry.Medianowy_3_5(3);
            EditImage.Source = img;
            editImageBit = img;
            MessageBox.Show("Zakończono!");
        }

        private void Medianowy_5_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Zostaniesz poinformowaniu po zakończeniu procesu");
            var img = Filtry.Medianowy_3_5(5);
            EditImage.Source = img;
            editImageBit = img;
            MessageBox.Show("Zakończono!");
        }
        #endregion
    }
}
