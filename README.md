#Zadanie z Biometri


#Zadanie 1

- Przygotuj program do przetwarzania plików graficznych (w wybranym języku programowania). Przez kolejne zajęcia będziesz rozbudowywał ten program, aby dodać do niego kolejne opcje - pamiętaj o tym przy projektowaniu. Program powinien mieć graficzny interfejs użytkownika i oferować następujące funkcjonalności:
[x] Wykonano
 
- Wczytaj i wyświetl wskazany przez użytkownika plik graficzny, obsługiwane formaty to jpeg, png, bmp, gif, tiff.
[x] Wykonano

- Wyświetl obraz w powiększeniu (np. x8)
[x] Wykonano

- Odczytaj wartość RGB wybranego piksela (np. wskazanego myszką).
[x] Wykonano

- Zmień wartość wskazanego piksela (np. po kliknięciu)
[x] Wykonano

- Zapisz plik wynikowy pod wskazaną nazwą i we wskazanym formacie
[x] Wykonano

******************* Zrobione **********************


#Zadanie 2


- Uzupełnij program z poprzednich zajęć o możliwość obliczenia i zaprezentowania (w postaci wykresu) histogramu wczytanego obrazu. W wypadku obrazów barwnych, oblicz osobne histogramy dla kanałów R, G i B oraz histogram dla wartości uśrednionych (R+G+B)/3. Następnie wykonaj operacje wykorzystujące LUT (lookup table):
[x] Wykonano
 
- Rozjaśnienie i przyciemnienie przy użyciu funkcji logarytmicznych lub kwadratowych
[x] Wykonano

- Rozciągnięcie histogramu (dla podanych wartości jasności a i b, ucinane są wartości poniżej a i powyżej b, a pozostała część rozciągana do pełnego zakresu jasności)
[x] Wykonano

- Wyrównanie histogramu (operacja przy użyciu histogramu skumulowanego jako LUT)
[x] Wykonano

- Aplikacja powinna umożliwiać zapis przekształconych obrazów.
[x] Wykonano

******************* Zrobione **********************

#Zadanie 3

- Uzupełnij program z poprzednich zajęć o możliwość wykonywania binaryzacji (progowania). Obrazy kolorowe przed operacją binaryzacji zamień na postać w skali szarości.
[x] Wykonano

- Binaryzacja z ręcznie wyznaczonym progiem - pozwól użytkownikowi wybrać wartość progu.
[x] Wykonano

- Automatyczne wyznaczanie progu - wyznacz próg przy pomocy metody Otsu. W metodzie Otsu próg wyznaczany jest przez minimalizację wariancji wewnątrzklasowej (lub maksymalizację wariancji międzyklasowej).
[x] Wykonano

- Binaryzacja lokalna - wykorzystując metodę Niblacka. W metodzie Niblacka prog wyznaczany jest osobno dla każdego piksela na podstawie średniej i wariancji wartości pikseli w jego otoczeniu. Pozwól użytkownikowi wybrać rozmiar okna oraz wartość k (parametr progowania).
[x] Wykonano

******************* Zrobione **********************
#Zadanie 4

- Dodaj do programu możliwość filtrowania obrazu:
[x] Wykonano

- Przy pomocy filtrów liniowych opartych o funkcję splotu (filtry konwolucyjne). Wykorzystaj maskę 3x3, dając uzytkownikowi możliwość wprowadzenia jej wartości. Przetestuj działanie na kilku standardowych operatorach: rozmywającym (filtr dolnoprzepustowy), Prewitta, Sobela, Laplace'a, wykrywający narożniki.
[x] Wykonano

- Przy pomocy filtru Kuwahara.
[x] Wykonano

- Przy pomocy filtru medianowego (dla masek 3x3 i 5x5).
[x] Wykonano

Pomoce: 
	- Prezentacja zamieszczona do zadania
	- http://www.algorytm.org/przetwarzanie-obrazow/filtrowanie-obrazow.html
	
******************* Zrobione **********************